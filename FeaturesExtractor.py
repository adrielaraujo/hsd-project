import os
import numpy as np
from SumDiffHistogram import SumDiffHistogram
from skimage.color import rgb2gray
from scipy.ndimage import imread
from sklearn import  preprocessing


class FeaturesExtractor:

    def __init__(self):
        self.origin = "UFFsax"
        self.images_dir = "Imagens\\" + self.origin + "\\"
        self.classes_file = "Imagens\\Relacao Classes\\UFF\\relacao_classes.csv"
        self.base_name = "HSD_"+self.origin + "_Dist1_21-07(2)"
        self.path_output = "Bases\\HSD\\" + self.origin + "\\" + self.base_name


        #self.extract_features(self.get_images_names(self.images_dir))
        self.extract_features(self.get_classes_images_UFPE(self.classes_file))
        self.normalize_base(self.path_output)




    def get_images_names(self, dir):
        images = {}

        for file in os.listdir(dir):
            if file.endswith(".png"):
                key, value = file.split("-")
                if key not in images:
                    images[key] = [value]
                else:
                    images[key].append(value)

        return self.get_classes_images(dir, images)


    def get_classes_images(self, dir, images):
        classes = {}
        classes = np.loadtxt(self.classes_file,delimiter=";")

        for id_img, classe in classes:
            key = "IR_" + str(int(id_img)).rjust(4, '0')
            images[key].append(classe)

        return images

    def get_images_names_UFPE(self, dir, images):
       # images = {}

        for file in os.listdir(dir):
            if file.endswith(".png"):
                key, value = file.split("-")
                if key in images:
                    images[key].append(value)
                # else:
                #     images[key].append(value)

        return images

    def get_classes_images_UFPE(self, dir):
        images = {}
        classes = np.loadtxt(dir,delimiter=";")

        for id_img, classe in classes:
            key = "IR_" + str(int(id_img)).rjust(4, '0')
            images[key] = [classe]


        return self.get_images_names_UFPE(self.images_dir, images)


    def extract_features(self, images):


        # 4 angulos considerados: (0,45,90,135)
        # Ditancia 1 para cada angulo (variacoes de Di e Dj, dao o valor da distancia)
        distances = {
            0: [0, 1],  # Dj da a distancia
            45: [1, 1],  # Di e Dj dao a distancia e ambos tem de ser iguais
            90: [1, 0],  # Di da a distancia
            135: [1, -1]  # Dj e DJ dao a distancia e o valor absoluto de ambos tem de ser igual
        }

        hsd = SumDiffHistogram()
        print(images)
        print("Extraindo características")
        #for i, j in images.items(): print(i, j)

        for key, value in images.items():
            current_img = np.loadtxt(self.images_dir + key + ".txt")

            classe = value[0]
            #print(imread(self.images_dir + key + "-" + value[1], flatten=True).shape)
            mask_right = rgb2gray(imread(self.images_dir + key + "-" + value[1], flatten=True))
            mask_left = rgb2gray(imread(self.images_dir + key + "-" + value[2], flatten=True))

            ### Gera as ROIS
            img_left = np.copy(current_img)
            img_right = np.copy(current_img)
            img_left[mask_left == 0] = 0
            img_right[mask_right == 0] = 0


            aux = [int(key.split('_')[1])]
            for angle, distance in distances.items():
                di, dj = distance[0], distance[1]

                features_left = hsd.calc(img_left, di, dj)
                features_right = hsd.calc(img_right, di, dj)


                aux += features_left + features_right
            aux.append(classe)

            features_img = np.array(aux)
            with open(self.path_output+".csv", 'ab') as stream:
                np.savetxt(stream, features_img[np.newaxis], delimiter=";", fmt='%.15f')

        print("Características extraidas com sucesso")




    def normalize_base(self, dir):
        print("Normalizando base")
        farray = np.loadtxt(dir+".csv", delimiter=';')
        X = farray[0:, 1:-1]

        min_max_scaler = preprocessing.MinMaxScaler()
        X = min_max_scaler.fit_transform(X)
        farray[0:, 1:-1] = X

        with open(self.path_output+"_Normalized.csv", 'ab') as stream:
            np.savetxt(stream, farray, delimiter=";", fmt='%.15f')

        print("Base normalizada com sucesso!")

if __name__ == "__main__":
    program = FeaturesExtractor()