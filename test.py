from skimage.data import binary_blobs, astronaut
from skimage.io import imread, imshow, imsave
#from skimage.viewer import ImageViewer
#from skimage.feature import greycoprops
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
from scipy.ndimage import imread
from timeit import default_timer as timer



class GLCM:

    def __init__(self, image, di,dj):

        offset = self.get_offset(image, di, dj)
        sum = image+offset
        diff = image-offset
        sum_occurences = self.count_occurences(sum)
        diff_occurences = self.count_occurences(diff)
        sum_vector = np.sort(np.reshape(sum, sum.size))
        diff_vector = np.sort(np.reshape(diff, sum.size))

if __name__ == '__main__':
    image = np.array([(0,0,0,0,0), (0,0,0,0,0), (1,1,1,1,1), (1,1,1,1,1)])
    #image = astronaut()
    # image = np.array([(1,2,3,4,5), (6,7,8,9,10), (11,12,13,14,15), (16,17,18,19,20)])

    image = imread("img.jpg")
    main = GLCM(image, 1,1)