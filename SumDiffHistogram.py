#from skimage.data import binary_blobs, astronaut
#from skimage.io import imread, imshow, imsave
#from skimage.viewer import ImageViewer
#from skimage.feature import greycoprops
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
from scipy.ndimage import imread
from timeit import default_timer as timer


class SumDiffHistogram:

    def __init__(self,image=None, di= None, dj = None):
        if image is not None and di is not None and dj is not None:
            self.calc(image, di, dj)
        self.flagPrepared = False


    def prepare_image(self, image, di, dj):
        offset = self.get_offset(image, di, dj)
        sum = image + offset
        diff = image - offset
        sum_occurences = self.count_occurences(sum)
        diff_occurences = self.count_occurences(diff)
        sum_vector = np.sort(np.reshape(sum, sum.size))
        diff_vector = np.sort(np.reshape(diff, sum.size))

        self.flagPrepared = True;

        return offset, sum, diff, sum_vector, diff_vector, sum_occurences, diff_occurences


    def calc(self, image, di, dj):
        offset, sum, diff, sum_vector, diff_vector, sum_occurences, diff_occurences = self.prepare_image(image,di,dj)

        # print("Di:", di, "  Dj:", dj)
        # print("Original:\n", image)
        # print("\nOffset:\n", offset)
        # print("\nSum:\n", sum, "\nDiff:\n", diff)
        # print("\nOccurences:\nSum:", sum_occurences, "\nDiff:", diff_occurences)
        # print("\nVectors:\nSum:", len(sum_vector), sum_vector, "\nDiff:", len(diff_vector), diff_vector)

        asm = self.calc_ASM(sum_vector, diff_vector)
        energy = self.calc_ENERGY(asm)
        contrast = self.calc_CONTRAST(diff_vector)
        homogeneity = self.calc_HOMOGENEITY(diff_vector)
        correlation = self.calc_CORRELATION(sum_vector, diff_vector)
        dissimilarity = self.calc_DISSIMILARITY(diff_vector)
        # print("\nASM:", asm)
        # print("ENERGY:", energy)
        # print("CONTRAST:", contrast)
        # print("HOMOGENEITY:", homogeneity)
        # print("CORRELATION:", correlation)
        # print("DISSIMILARITY:", dissimilarity)

        features = [asm, energy, contrast, homogeneity, correlation, dissimilarity]

        return features

    def get_offset(self, image, di,dj):

        ''' %DO: Corrigir geracao do offset para angulo 135'''

        offset = np.zeros_like(image)
        if dj >=0:
            cutted = image[di:, dj:]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]

            offset[:rows, :cols] = offset[:rows, :cols] + cutted
        else:
            cutted = image[di:, :dj]
            shape = cutted.shape
            rows = shape[0]
            cols = shape[1]
            #of[:tam-di, abs(dj):
            offset[:rows-di+1, abs(dj):] = offset[:rows-di+1, abs(dj):] + cutted

        return offset
        # if dj <0: cutted = image[di:,:dj]
        # else: cutted = image[di:,dj:]
        # # print("\ncutted\n", cutted)
        # offset = np.zeros_like(image)
        #
        # shape = cutted.shape
        # rows = shape[0]
        # cols = shape[1]
        #
        # offset[:rows, :cols] = offset[:rows, :cols] + cutted
        # if dj>=0: offset[:rows,:cols] = offset[:rows,:cols]+ cutted
        # else: offset[:rows,abs(dj):] = offset[:rows,abs(dj):] + cutted
        #
        # return offset

    def count_occurences(self,image):
        unique, counts = np.unique(image, return_counts=True)
        return dict(zip(unique, counts))

    def get_histogram(self, image, title):
        N = len(image)
        plt.bar(range(N), image.values(), align='center')#, color=jet(np.linspace(0, 1.0, N)))
        plt.xticks(range(N), image.k())
        plt.title(title)
        plt.ylabel("Frequency")
        plt.xlabel("Value")
        plt.show()



    # VALIDAR IMPLEMENTACAO DE EQUACOES APRESENTADAS POR ACORDO COM UNSER,86 (PAPER NO REPOSITORIO)

    def calc_ASM(self, sum_histogram, diff_histogram):

        #### USING PYTHON METHODS
        # start = timer()
        # sum_s, sum_d = 0,0
        # for pi in sum_histogram:
        #     sum_s += pi**2
        # for pj in diff_histogram:
        #     sum_d += pj**2
        # result =sum_d * sum_s
        # end = timer()
        # print("Time using python methods: ", end - start)


        #### USING NUMPY METHODS
        #start = timer()
        sum_to2 = np.multiply(sum_histogram, sum_histogram)
        diff_to2 = np.multiply(diff_histogram,diff_histogram)

        sum_s = np.sum(sum_to2)
        diff_s = np.sum(diff_to2)
        result = np.multiply(sum_s, diff_s)
        #end = timer()
        #print("Time using numpy methods: ", end - start)

        return result

    def calc_ENERGY(self, asm):
        return sqrt(asm)

    def calc_CONTRAST(self, diff_histogram):

        #### USING PYTHON METHODS
        # result = 0
        # for j in range(len(diff_histogram)):
        #     result += (j**2) * diff_histogram[j]

        #### USING NUMPY METHODS
        indexes = np.arange(len(diff_histogram))
        indexes_to2 = np.multiply(indexes,indexes)
        multiply = np.multiply(indexes_to2, diff_histogram)
        result = np.sum(multiply)

        return result

    def calc_HOMOGENEITY(self, diff_histogram):

        #### USING PYTHON METHODS
        # result = 0
        # for j in range(len(diff_histogram)):
        #     result += (diff_histogram[j]/(1+(j**2)))

        #### USING NUMPÝ METHODS
        indexes = np.arange(len(diff_histogram))
        indexes_to2 = np.multiply(indexes, indexes)
        division = np.divide(diff_histogram,1+indexes_to2)
        result = np.sum(division)

        return result

    def calc_CORRELATION(self,sum_histogram, diff_histogram):

        #### USING PYTHON METHODS
        # start = timer()
        # result_sum = 0
        # sum_mean = np.mean(sum_histogram)
        # for i in range(len(sum_histogram)):
        #     result_sum += ((i-(2 * sum_mean))**2) * sum_histogram[i]
        #
        # result_diff = self.calc_CONTRAST(diff_histogram)
        #
        # correlation = 0.5*(result_sum - result_diff)
        # end = timer()
        # print("Time using python methods: ", end - start)


        #### USING NUMPY METHODS
        #start = timer()
        indexes = np.arange(len(diff_histogram))
        sum_mean = np.mean(sum_histogram)
        sum_mean_x2 = np.multiply(sum_mean, 2)
        aux = np.subtract(indexes,sum_mean_x2)
        i_x_sum_mean_to2 = np.multiply(aux,aux)

        result_sum = np.multiply(i_x_sum_mean_to2, sum_histogram)
        result_diff = self.calc_CONTRAST(diff_histogram)

        aux = np.subtract(result_sum, result_diff)
        aux = np.multiply(0.5, aux)
        correlation = np.sum(aux)
        #end = timer()
        #print("Time using numpy methods: ", end - start)
        return correlation

    def calc_DISSIMILARITY(self,diff_histogram):

        #### USING PYTHON METHODS
        # dissimilarity = 0
        # for j in range(len(diff_histogram)):
        #     dissimilarity += j * diff_histogram[j]

        #### USING NUMPY METHODS
        indexes = np.arange(len(diff_histogram))
        dissimilarity = np.multiply(indexes,diff_histogram)
        dissimilarity = np.sum(dissimilarity)


        return dissimilarity

if __name__ == '__main__':
    image = np.array([(0,0,0,0,0), (0,0,0,0,0), (1,1,1,1,1), (1,1,1,1,1)])
    #image = astronaut()
    #image = np.array([(1,2,3,4,5), (6,7,8,9,10), (11,12,13,14,15), (16,17,18,19,20)])

    #image = imread("img.jpg")
    main = SumDiffHistogram(image, 1,-1)