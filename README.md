# HSD Project #

### Este repositório contem implementações que: ###
* Geram Histogramas de Soma, dada uma imagem;
* Geram Histogramas de Diferença, dada uma imagem;
* Calculam os seguintes descritores (baseando-se nos histogramas):
     * Segundo Momento Angular (SMA)
     * Energia
     * Contraste
     * Homogeneidade
     * Correlação 
     * Dissimilaridade
* Geram bases de características a partir destes descritores considerando 4 diferentes ângulos (0º, 45º, 90º e 180º)
* Normaliza bases de características
* Gera vários modelos a partir destas bases considerando um conjunto de variações possiveis para alguns parametros do classificador (Kernel, Gamma, Coef0, Tol)
* Salva em log os resultados dos modelos para posterior análise



#### Observações a considerar: ####
* SMA - Fórmula da Energia em (UNSER, 86)
* Energia - Raiz Quadrada do SMA
* Dissimilaridade: Não apresentada por Unser, porém observando as equações usadas no Scikit nota-se que a dissimilaridade é similar ao contraste, porém com
crescimento linear e não exponencial, portanto, a implementação da dissimilaridade é uma variação linear do contraste.


### Dependências ###

* Matplotlib *(caso plot gráficos)*
* Numpy

*Usando Python 3*