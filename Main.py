from SumDiffHistogram import SumDiffHistogram
from scipy.ndimage import imread
from skimage.color import rgb2gray
import numpy as np
import matplotlib.pyplot as plt
from sklearn import  preprocessing
import numpy as np
from sklearn.model_selection import train_test_split
import csv
from sklearn import svm, metrics

import os

class Main:

    def __init__(self):

        dir_imgs = "E:\\Adriel\\UFF\\Disciplinas\\Estudo Orientado\\HSD Project [git]\\Imagens\\UFFsax\\Termogramas\\"
        dir_classes = "E:\\Adriel\\UFF\\Disciplinas\\Estudo Orientado\HSD Project [git]\\Imagens\\UFFsax\\Relacao Classes\\relacao_classes.csv"
        self.path_features = "Bases\\HSD\\UFFsax\\"
        self.name_base = "HSD_UFFSax_Dist1_01-07.csv"
        self.name_normalized = "HSD_UFFSax_Dist1_01-07_Normalized.csv"
        self.name_log = "HSD_UFFSax_Dist1_01-07_Normalized"

        #self.normalize_base(self.path_features+self.name_base)

        X_train, X_test, y_train, y_test = self.divide_base(self.path_features+self.name_normalized)

        self.bruteForceSVM( X_train, X_test, y_train, y_test)

    def saveCSV(self,arq, features):
        with open(arq, 'a') as csvfile:
            temp = csv.writer(csvfile, delimiter=';', dialect='excel')
            temp.writerow(features)

    def bruteForceSVM(self, treino, teste, classesTreino, classesTeste):

        print("tamTRE", len(treino))
        print("tamTE", len(teste))
        print("claTRE", len(classesTreino))
        print("claTe", len(classesTeste))


        KERNEL = ["linear", "poly", "rbf", "sigmoid"]
        GAMMA = [0.0001, 0.0010, 0.0100, 0.100]
        COEF0 = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
        TOL = [0.0001, 0.0010, 0.0100, 0.100]
        NU = [0.3, 0.4, 0.5, 0.6, 0.7]

        GAMMA = [0.000000000001, 0.00000000001, 0.0000000001, 0.00000001, 0.0000001, 0.000001, 0.00001, 0.0001, 0.0010,
                 0.0100, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0, 5.0,  6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0]
        COEF0 = [0.0001, 0.001, 0.01,  0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0, 5.0,  6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0]
        TOL = [0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001, 0.00000001]


        log = "Logs\\HSD\\UFFsax\\log_ClassificacaoSVC_" + self.name_log + "teste2"
        self.saveCSV(log, ['######VARIAÇÕES CONSIDERADAS:'])
        self.saveCSV(log, ['KERNEL:', KERNEL])
        self.saveCSV(log, ['GAMMA:', GAMMA])
        self.saveCSV(log, ['COEF0:', COEF0])
        self.saveCSV(log, ['TOL:', TOL])
        self.saveCSV(log, ['NU:', NU, "Apenas para nu-SVC"])

        self.saveCSV(log, ["\n\n\n#################### C-SVC#########"])

        cont = 0
        for k in KERNEL:
            for g in GAMMA:
                for c in COEF0:
                    for t in TOL:
                        cont+=1
                        # Create a classifier: a support vector classifier
                        classifier = svm.SVC(kernel=k, coef0=c, gamma=g, tol=t)

                        # We learn the digits on the first half of the digits
                        classifier.fit(treino, classesTreino)

                        # Now predict the value of the digit on the second half:
                        expected = classesTeste
                        predicted = classifier.predict(teste)

                        aux = [
                            "\n\n-------------------------------------------------------------------------------------------------------\n"
                            "Classification report for classifier %s:\n%s\n"
                            % (classifier, metrics.classification_report(expected, predicted))]
                        self.saveCSV(log, aux)
                        # print(aux)

                        aux = ["Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted)]
                        self.saveCSV(log, aux)
                        # print(aux)

        # log = "Logs\\HSD\\UFFsax\\log_ClassificacaoNuSVC_" + self.name_log +
        # self.saveCSV(log, ['######VARIAÇÕES CONSIDERADAS:'])
        # self.saveCSV(log, ['KERNEL:', KERNEL])
        # self.saveCSV(log, ['GAMMA:', GAMMA])
        # self.saveCSV(log, ['COEF0:', COEF0])
        # self.saveCSV(log, ['TOL:', TOL])
        # self.saveCSV(log, ['NU:', NU, "Apenas para nu-SVC"])
        # self.saveCSV(log, ["\n\n\n\n\n#################### nu-SVC#########"])
        # for k in KERNEL:
        #     for g in GAMMA:
        #         for c in COEF0:
        #             for t in TOL:
        #                 for n in NU:
        #                     # Create a classifier: a support vector classifier
        #                     classifier = svm.NuSVC(nu=n, kernel=k, gamma=g, coef0=c, tol=t)
        #
        #                     # We learn the digits on the first half of the digits
        #                     classifier.fit(treino, classesTreino)
        #
        #                     # Now predict the value of the digit on the second half:
        #                     expected = classesTeste
        #                     predicted = classifier.predict(teste)
        #                     aux = [
        #                         "\n\n-------------------------------------------------------------------------------------------------------------\n"
        #                         "Classification report for classifier %s:\n%s\n"
        #                         % (classifier, metrics.classification_report(expected, predicted))]
        #                     self.saveCSV(log, aux)
        #                     print(aux)
        #
        #
        #                     aux = ["Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted)]
        #                     self.saveCSV(log, aux)
        #                     print(aux)



    def divide_base(self, dir):

        farray = np.loadtxt(dir, delimiter=';')
        X = farray[0:, 1:-1]
        y = farray[0:, -1]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 42)

        return X_train, X_test, y_train, y_test



    def normalize_base(self, dir):
        farray = np.loadtxt(dir, delimiter=';')
        X = farray[0:, 1:-1]
        y = farray[0:, -1]
        id = farray[0:, 0]
        print(id)


        min_max_scaler = preprocessing.MinMaxScaler()
        X = min_max_scaler.fit_transform(X)
        farray[0:, 1:-1] = X

        with open(self.path_features+"HSD_UFFSax_Dist1_01-07_Normalized.csv", 'ab') as stream:
            np.savetxt(stream, farray, delimiter=";", fmt='%.15f')



    def extract_features(self):

        # 4 angulos considerados: (0,45,90,135)
        # Ditancia 1 para cada angulo (variacoes de Di e Dj, dao o valor da distancia)
        distances = {
            0: [0, 1],  # Dj da a distancia
            45: [1, 1],  # Di e Dj dao a distancia e ambos tem de ser iguais
            90: [1, 0],  # Di da a distancia
            135: [1, -1]  # Dj e DJ dao a distancia e o valor absoluto de ambos tem de ser igual
        }

        images = {}
        for file in os.listdir(dir_imgs):
            if file.endswith(".png"):
                key, value = file.split("-")
                if key not in images:
                    images[key] = [value]
                else:
                    images[key].append(value)

                    # print("ss,", images)
        self.images = images
        self.read_classes_images(dir_classes)
        hsd = SumDiffHistogram()
        for key, value in self.images.items():
            current_img = np.loadtxt(dir_imgs + key + ".txt")

            mask_left = rgb2gray(imread(dir_imgs + key + "-" + value[0]))
            mask_right = rgb2gray(imread(dir_imgs + key + "-" + value[1]))
            classe = value[2]

            ### Gera as ROIS
            img_left = np.copy(current_img)
            img_right = np.copy(current_img)
            img_left[mask_left == 0] = 0
            img_right[mask_right == 0] = 0
            # plt.imshow(img_left)
            # plt.show()
            # plt.imshow(img_right)
            # plt.show()



            aux = [int(key.split('_')[1])]
            for angle, distance in distances.items():
                di, dj = distance[0], distance[1]
                features_left = hsd.calc(img_left, di, dj)
                features_right = hsd.calc(img_right, di, dj)

                aux += features_left + features_right
            aux.append(classe)
            print(aux)

            features_img = np.array(aux)
            with open(path_features + name_base, 'ab') as stream:
                np.savetxt(stream, features_img[np.newaxis], delimiter=";", fmt='%.15f')

                # for angle, distance in distances.items():
                #     di, dj = distance[0],distance[1]
                #     features_left = hsd.calc(img_left, di, dj)
                #     features_right = hsd.calc(img_right,di,dj)
                #
                #     features_img = np.append(features_left, features_right)
                #
                # with open(path_features + file, 'ab') as stream:
                #     np.savetxt(stream, features_img[np.newaxis], delimiter=";")



                # print (features_img)

        print("FIM")

    def read_classes_images(self, dir):
        classes = {}

        classes = np.loadtxt(dir,delimiter=";")
      #  print("\n\nclasses:\n",classes)

        for id_img, classe in classes:
            key = "IR_" + str(int(id_img)).rjust(4, '0')
            self.images[key].append(classe)

        #print(self.images)



    def getRoi(self, img, mask):

        img[mask>0]=0
        return img



    def divideImage(self, image, nx, ny):
        image_divided = []
        rows, cols = image.shape

        limit_cell_rows = rows/nx
        limit_cell_cols = cols/ny

        cont_rows, cont_cols, aux_rows, aux_cols = 0,0,0,0

        for r in range(limit_cell_rows):
            aux_rows += limit_cell_rows
            for c in range(limit_cell_cols):
                aux_cols += limit_cell_cols

                image_aux = image[cont_rows:limit_cell_rows+cont]





if __name__ == "__main__":
    program = Main()