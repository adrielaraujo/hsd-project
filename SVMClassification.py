from sklearn.model_selection import train_test_split
import csv
from sklearn import svm, metrics
import numpy as np



class Classify:

    def __init__(self):
        "HSD_UFPEcax_Dist1_21-07_desbalanceada_Normalized"
        self.origim = "UFPEsax"
        self.data_base = "21-07"
        self.base_name = "\\HSD_"+ self.origim + "_Dist1_" + self.data_base + "_desbalanceada_Normalized.csv"
        self.base_dir = "Bases\\HSD\\"+self.origim + self.base_name
        #self.base_dir = "Bases\\HSD\\" + self.origim + "\\HSD_UFFSax_Dist1_01-07_Normalized.csv"
        X_train, X_test, y_train, y_test = self.divide_base(self.base_dir)
        self.bruteForceSVM(X_train, X_test, y_train, y_test)


    def divide_base(self, dir):
        farray = np.loadtxt(dir, delimiter=';')
        X = farray[0:, 1:-1]
        y = farray[0:, -1]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state = 42)

        return X_train, X_test, y_train, y_test


    def saveCSV(self,arq, features):
        with open(arq, 'a') as csvfile:
            temp = csv.writer(csvfile, delimiter=';', dialect='excel')
            temp.writerow(features)

    def bruteForceSVM(self, treino, teste, classesTreino, classesTeste):
        print(self.base_dir)
        print("tamTRE", len(treino))
        print("tamTE", len(teste))
        print("claTRE", len(classesTreino))
        print("claTe", len(classesTeste))


        KERNEL = ["linear", "poly", "rbf", "sigmoid"]
        NU = [0.3, 0.4, 0.5, 0.6, 0.7]
        GAMMA = [0.000000000001, 0.00000000001, 0.0000000001, 0.00000001, 0.0000001, 0.000001, 0.00001, 0.0001, 0.0010,
                 0.0100, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0, 5.0,  6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0]
        COEF0 = [0.0001, 0.001, 0.01,  0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0, 5.0,  6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0]#, 13.2, 13.5, 13.7, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0]
        TOL = [0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001, 0.00000001]


        "HSD_UFFsax_Dist1_21-07(2)"
        log = "Logs\\HSD\\" + self.origim + "\\logClassificacao_SVC_HSD_" + self.origim + "_Dist1_" + self.data_base + "_desbalanceada_Normalized"

        self.saveCSV(log, [self.base_name])
        self.saveCSV(log, ['######VARIAÇÕES CONSIDERADAS:'])
        self.saveCSV(log, ['KERNEL:', KERNEL])
        self.saveCSV(log, ['GAMMA:', GAMMA])
        self.saveCSV(log, ['COEF0:', COEF0])
        self.saveCSV(log, ['TOL:', TOL])
        self.saveCSV(log, ['NU:', NU, "Apenas para nu-SVC"])

        self.saveCSV(log, ["\n\n\n#################### C-SVC#########"])

        cont = 0
        for k in KERNEL:
            for g in GAMMA:
                for c in COEF0:
                    for t in TOL:
                        cont+=1
                        # Create a classifier: a support vector classifier
                        classifier = svm.SVC(kernel=k, coef0=c, gamma=g, tol=t)

                        # We learn the digits on the first half of the digits
                        classifier.fit(treino, classesTreino)

                        # Now predict the value of the digit on the second half:
                        expected = classesTeste
                        predicted = classifier.predict(teste)

                        aux = [
                            "\n\n-------------------------------------------------------------------------------------------------------\n"
                            "Classification report for classifier %s:\n%s\n"
                            % (classifier, metrics.classification_report(expected, predicted))]
                        self.saveCSV(log, aux)
                        # print(aux)

                        aux = ["Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted)]
                        self.saveCSV(log, aux)
                        # print(aux)

        # log = "Logs\\HSD\\" + self.origim + "\\logClassificacao_NuSVC_HSD_" + self.origim + "_Dist1_01-07_Normalized_ufpesax"
        # self.saveCSV(log, [self.base_name])
        # self.saveCSV(log, ['######VARIAÇÕES CONSIDERADAS:'])
        # self.saveCSV(log, ['KERNEL:', KERNEL])
        # self.saveCSV(log, ['GAMMA:', GAMMA])
        # self.saveCSV(log, ['COEF0:', COEF0])
        # self.saveCSV(log, ['TOL:', TOL])
        # self.saveCSV(log, ['NU:', NU, "Apenas para nu-SVC"])
        # self.saveCSV(log, ["\n\n\n\n\n#################### nu-SVC#########"])
        # for k in KERNEL:
        #     for g in GAMMA:
        #         for c in COEF0:
        #             for t in TOL:
        #                 for n in NU:
        #                     # Create a classifier: a support vector classifier
        #                     classifier = svm.NuSVC(nu=n, kernel=k, gamma=g, coef0=c, tol=t)
        #
        #                     # We learn the digits on the first half of the digits
        #                     classifier.fit(treino, classesTreino)
        #
        #                     # Now predict the value of the digit on the second half:
        #                     expected = classesTeste
        #                     predicted = classifier.predict(teste)
        #                     aux = [
        #                         "\n\n-------------------------------------------------------------------------------------------------------------\n"
        #                         "Classification report for classifier %s:\n%s\n"
        #                         % (classifier, metrics.classification_report(expected, predicted))]
        #                     self.saveCSV(log, aux)
        #                    # print(aux)
        #
        #
        #                     aux = ["Confusion matrix:\n%s" % metrics.confusion_matrix(expected, predicted)]
        #                     self.saveCSV(log, aux)
        #                     #print(aux)


if __name__ == "__main__":
    program = Classify()